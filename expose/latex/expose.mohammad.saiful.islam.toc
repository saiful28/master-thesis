\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Introduction}{1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Research Questions}{1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Structure of the Thesis}{1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}Example citation \& symbol reference}{1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.4}Example reference}{2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.5}Example image}{2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.6}Example table}{2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Background}{3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Methods}{4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Results}{5}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Discussion}{6}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {6}Conclusion}{7}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Appendix \numberline {A}Code}{8}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Appendix \numberline {B}Math}{9}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Appendix \numberline {C}Dataset}{10}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline Eidesstattliche Erkl\"arung}{11}
